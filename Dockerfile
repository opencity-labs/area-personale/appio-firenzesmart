# Usa l'immagine di Python 3.11
FROM python:3.11

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

ENV TZ=Europe/Rome

# Install pip requirements
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt &&  \
    pip install --no-cache-dir cloudstorage[amazon] &&  \
    pip install --no-cache-dir cloudstorage[minio] &&  \
    pip install --no-cache-dir cloudstorage[local]

# Copia i file necessari nel container
COPY . /app

# Imposta la working directory nel container
WORKDIR /app/src

# Esegui lo script quando il container viene avviato
CMD ["python3", "main.py" ]
