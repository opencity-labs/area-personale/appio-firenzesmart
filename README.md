# AppIO Firenze Smart Proxy

## Cosa fa
Questo servizio si occupa di fare da intermediario tra Opencity Italia - Area Personale e l'integrazione custom di AppIO di Firenze Smart

## Stadio di sviluppo
Il servizio è nello stadio di Stable 1.2.4


## Come si usa
Da terminale:
1. `git clone https://gitlab.com/opencity-labs/area-personale/appio-firenzesmart.git`
2. `cd appio-proxy`
3. `docker-compose up -d` (assicurarsi che kafka sia su, in caso contrario digitare `docker-compose up -d kafka`)
4. Accedere a `kafka-ui` via `http://localhost:8080`
5. Accedere al topic `messages` e andare nella sezione Messages
6. Copiare il messaggio presente nel topic
7. Premere "Produce Message"
8. Verificare dai log che il messaggio venga inviato correttamente


## Configurazione
### Configurazione variabili d'ambiente
| Nome                    | Default                                  | Descrizione                                                                       |
|-------------------------|------------------------------------------|-----------------------------------------------------------------------------------|
| APPIO_ENDPOINT          | https://apigw-staging.055055.it/postmsg  | Url da chaimare per inviare i messaggi su AppIO                                   |
| KAFKA_TOPIC_NAME        | messages                                 | Topic di kafka da cui si leggono i pagamenti da processare                        |
| KAFKA_BOOTSTRAP_SERVERS | kafka:9092                               | Endpoint di kafka                                                                 |
| KAFKA_GROUP_ID          | appio_proxy                              | Nome del consumer group                                                           |
| SDC_AUTH_TOKEN_USER     | user                                     | Nome utente per chiamare API Area Personale                                       |
| SDC_AUTH_TOKEN_PASSWORD | password                                 | Password per chiamare API Area Personale                                          |
| STORAGE_TYPE            | MINIO                                    | Tipo di storage della configurazione                                              |
| STORAGE_KEY             | AKIAIOSFODNN7EXAMPLE                     | Chiave dello storage dei pagamenti e delle configurazioni di tenant e servizio    |
| STORAGE_SECRET          | wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY | Password dello storage dei pagamenti e delle configurazioni di tenant e servizio  |
| STORAGE_BUCKET_NAME     | config                                   | Nome dello storage dei pagamenti e delle configurazioni di tenant e servizio      |
| STORAGE_ENDPOINT        | minio:9000                               | Endpoint dello storage dei pagamenti e della configurazioni di tenanto e servizio |
| ENV                     | QA                                       | Ambiente in cui il servizio è in esecuzione                                       |

### Esempio Configurazione
```json
{
  "config": {
    "publisher_code": "opencity",
    "channel_code": "AppIO"
  },
  "tenants": [
    {
      "id": "60e35f02-1509-408c-b101-3b1a28109329",
      "api_url": "https://servizi.comune-qa.bugliano.pi.it/lang/api",
      "name": "FIRENZE",
      "generate_id": true,
      "services": [
        {
          "service_code": "istruzione",
          "collection_id": "27874fcd-33ba-4db4-9bdc-d86252551a32"
        }
      ]
    }
  ]
}
```

## Sast

## Dependency Scanning

## Renovate

## Coding Style
