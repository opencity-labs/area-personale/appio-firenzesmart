import json
import unittest
from unittest.mock import MagicMock, patch

from main import run


class TestMain(unittest.TestCase):
    @patch('main.uuid')
    @patch('main.KafkaConsumer')
    @patch('main.SDCClient')
    @patch('main.requests.post')
    @patch('main.open')
    @patch(
        'main.os.environ',
        {
            'APPIO_ENDPOINT': 'mock_endpoint',
            'KAFKA_BOOTSTRAP_SERVERS': 'mock_bootstrap_servers',
            'KAFKA_TOPIC_NAME': 'mock_topic_name',
            'KAFKA_GROUP_ID': 'mock_group_id',
            'SDC_AUTH_TOKEN_USER': 'mock_username',
            'SDC_AUTH_TOKEN_PASSWORD': 'mock_password',
        },
    )
    def test_run(self, mock_open, mock_post, mock_sdc_client, mock_kafka_consumer, mock_uuid):
        mock_uuid.uuid4.return_value = 'mock_generated_id'

        mock_kafka_consumer_instance = MagicMock()
        mock_kafka_consumer.return_value = mock_kafka_consumer_instance

        mock_sdc_client_instance = MagicMock()
        mock_sdc_client_instance.get_user.return_value = 'mock_fiscal_code'
        mock_sdc_client.return_value = mock_sdc_client_instance

        mock_response = MagicMock()
        mock_response.json.return_value = {'id_msg': [{'id_msg': 'mock_message_id'}]}
        mock_post.return_value = mock_response

        mock_configuration = {
            'config': {
                'channel_code': 'mock_channel_code',
                'publisher_code': 'mock_publisher_code',
            },
            'tenants': [
                {
                    'id': 'mock_tenant_id',
                    'api_url': 'mock_api_url',
                    'name': 'mock_tenant_name',
                    'generate_id': True,
                    'services': [
                        {
                            'collection_id': 'mock_collection_id',
                            'service_code': 'mock_service_code',
                        },
                    ],
                },
            ],
        }

        # Mock del file di configurazione
        mock_open.return_value.__enter__().read.return_value = json.dumps(mock_configuration)

        # Mock del messaggio Kafka
        mock_message_data = {
            'id': 'mock_message_id',
            'related_entity_id': 'mock_related_entity_id',
            'tenant_id': 'mock_tenant_id',
            'remote_collection': {'id': 'mock_collection_id'},
            'receivers_ids': ['mock_user_id'],
            'transmission_type': 'outbound',
            'email': 'mock_email',
            'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt',
            'subject': 'mock_subject',
            'read_at': None,
        }
        mock_message = MagicMock()
        mock_message.value.decode.return_value = json.dumps(mock_message_data)

        # Esegui lo script
        run(mock_message_data, mock_configuration, 'mock_endpoint')

        # Verifica che la funzione requests.post sia stata chiamata correttamente
        mock_post.assert_called_once_with(
            'mock_endpoint',
            json={
                'canale': 'mock_channel_code',
                'ente': 'mock_tenant_name',
                'messaggi': [
                    {
                        'codice_fiscale': 'mock_fiscal_code',
                        'email': 'mock_email',
                        'id_record_pubblicatore': 'mock_generated_id',
                        'metadata': [],
                        'testo': 'Lorem ipsum dolor sit amet, '
                        'consectetur adipiscing elit, sed do eiusmod tempor incididunt',
                        'titolo': 'mock_subject',
                    },
                ],
                'pubblicatore': 'mock_publisher_code',
                'servizio': 'mock_service_code',
            },
        )


if __name__ == '__main__':
    unittest.main()
