import json
import os
import uuid
from functools import cached_property

import requests
from cachetools import TTLCache, cached
from dotenv import load_dotenv
from kafka import KafkaConsumer, OffsetAndMetadata, TopicPartition
from prometheus_client import Counter, Histogram, start_http_server

from file_manager import FileManager
from logger import appio_logger as logger

load_dotenv()

appio_endpoint = os.environ.get('APPIO_ENDPOINT')
kafka_bootstrap_servers = os.environ.get('KAFKA_BOOTSTRAP_SERVERS')
kafka_topic_name = os.environ.get('KAFKA_TOPIC_NAME')
kafka_group_id = os.environ.get('KAFKA_GROUP_ID')
sdc_auth_token_username = os.environ.get('SDC_AUTH_TOKEN_USER')
sdc_auth_token_password = os.environ.get('SDC_AUTH_TOKEN_PASSWORD')
env = os.environ.get('ENV', 'LOCAL')
event_version = 2
app_name = 'appio-proxy'


# Definizione delle metriche Prometheus
oc_appio_success_events_total = Counter(
    'oc_appio_success_events_total',
    'Total number of successful events processed by APPIO',
    ('env', 'app_name'),
)
oc_appio_provider_errors_total = Counter(
    'oc_appio_provider_errors_total',
    'Total number of errors encountered during APPIO provider requests',
    ('env', 'app_name'),
)
oc_appio_internal_errors_total = Counter(
    'oc_appio_internal_errors_total',
    'Total number of internal errors encountered during app initialization',
    ('env', 'app_name'),
)
oc_appio_provider_latency = Histogram(
    'oc_appio_provider_latency',
    'Latency of APPIO provider requests',
    ('env', 'app_name'),
    buckets=(0.1, 0.25, 0.5, 0.75, 1.0, 2.5, 5.0, 7.5, 10.0, 15.0, 20.0, 30.0),
)


class SDCClient:
    def __init__(self, base_url, username, password):
        self.base_url = base_url
        self.username = username
        self.password = password

    @cached_property
    def _token(self):
        logger.debug(f'Getting authentication token for tenant with domain {self.base_url}')
        response = requests.post(
            f'{self.base_url}/auth',
            json={'username': self.username, 'password': self.password},
        )
        response.raise_for_status()
        response = response.json()
        logger.debug(response)
        return response['token']

    @cached(cache=TTLCache(maxsize=1024, ttl=600))
    def get_user(self, user_id):
        logger.debug(f'Getting fiscal code for user with id {user_id}')
        user_url = f'{self.base_url}/users/{user_id}'
        headers = {'Authorization': f'Bearer {self._token}'}
        response = requests.get(user_url, headers=headers)
        response.raise_for_status()  # Raise an exception if the request is not successful
        return response.json().get('codice_fiscale')


# Funzione per controllare i dati del messaggio e restituire l'id del tenant e la collection_id se trovati
def check_message_data(msg, config):
    tenant_id = msg.get('tenant_id')
    collection_id = msg.get('remote_collection', {}).get('id')
    user_id = msg.get('receivers_ids')[0] if msg.get('receivers_ids') else None
    transmission_type = msg.get('transmission_type')
    read_at = msg.get('read_at')

    # Controlla se il messaggio è inviato dall'area personale verso il cittadino e non è già stato letto
    if transmission_type == 'outbound' and not read_at:
        # Controlla se i tenant_id e collection_id sono presenti nella configurazione
        for tenant in config.get('tenants', []):
            if tenant.get('id') == tenant_id:
                api_url = tenant.get('api_url')
                for service in tenant.get('services', []):
                    if service.get('collection_id') == collection_id:
                        return tenant_id, collection_id, user_id, api_url
    return None, None, None, None


# Funzione per creare il payload dal messaggio Kafka
def create_payload(msg, config, tenant_id, collection_id, fiscal_code):
    tenant = next((t for t in config['tenants'] if t['id'] == tenant_id), None)
    service = next((s for s in tenant['services'] if s['collection_id'] == collection_id), None)

    if tenant is None or service is None:
        return None

    generate_id = tenant.get('generate_id', False)

    # Creazione del payload
    payload = {
        'canale': config['config']['channel_code'],
        'ente': tenant['name'],
        'messaggi': [
            {
                'codice_fiscale': fiscal_code,
                'email': msg.get('email', ''),
                'id_record_pubblicatore': str(uuid.uuid4()) if generate_id else None,
                'metadata': [],
                'testo': msg.get('message', ''),
                'titolo': msg.get('subject', ''),
            },
        ],
        'pubblicatore': config['config']['publisher_code'],
        'servizio': service['service_code'],
    }
    return payload


# Funzione per inviare la richiesta HTTP POST all'endpoint APPIO
@oc_appio_provider_latency.labels(env, app_name).time()
def send_payload(payload, url):
    try:
        response = requests.post(url, json=payload)
        response.raise_for_status()  # Raise an exception if the request is not successful
        oc_appio_success_events_total.labels(env, app_name).inc()  # Incrementa il contatore degli eventi di successo
        logger.info(f'Message created successfully with id: {response.json()["id_msg"][0]["id_msg"]}')
    except requests.exceptions.RequestException as rex:
        # Incrementa il contatore degli errori del provider APPIO
        oc_appio_provider_errors_total.labels(env, app_name).inc()
        logger.error('Error sending POST request:', rex.response.content)


def run(msg, config, endpoint):
    logger.debug(f'Read event with id {msg["id"]} from application {msg["related_entity_id"]}')

    # Check the message data
    tenant_id, collection_id, user_id, api_url = check_message_data(msg, config)
    if tenant_id and collection_id and user_id:
        logger.info(
            f'Event {msg["id"]} from application {msg["related_entity_id"]}: Processing',
        )

        # Configure SDC Client Data
        client = SDCClient(
            base_url=api_url,
            username=sdc_auth_token_username,
            password=sdc_auth_token_password,
        )

        # Get the codice_fiscale using SDCClient
        fiscal_code = client.get_user(user_id)

        # Create the payload for the HTTP POST request
        payload = create_payload(msg, config, tenant_id, collection_id, fiscal_code)

        if payload:
            # Send the HTTP POST request
            send_payload(payload, endpoint)
        else:
            logger.warning('Error creating payload, message ignored.')
    else:
        logger.debug(
            f'Event {msg["id"]} from application {msg["related_entity_id"]}: ' f'Processing not required',
        )


if __name__ == '__main__':
    try:
        # Start HTTP server to expose Prometheus metrics
        start_http_server(8000)

        # Configure the Kafka consumer
        consumer = KafkaConsumer(
            kafka_topic_name,
            bootstrap_servers=kafka_bootstrap_servers,
            group_id=kafka_group_id,
            enable_auto_commit=False,
        )

        # Read the configuration from the JSON file
        configuration = json.loads(FileManager().get_file('config.json'))

        # Run the consumer
        logger.info('Starting to consume...')
        for message in consumer:
            try:
                message_data = json.loads(message.value.decode('utf-8'))
                if message_data.get('event_version') == event_version:
                    run(message_data, configuration, appio_endpoint)
                else:
                    logger.debug('Event version not supported, message skipped')
                tp = TopicPartition(topic=message.topic, partition=message.partition)
                om = OffsetAndMetadata(offset=message.offset + 1, metadata='')
                consumer.commit(offsets={tp: om})
            except Exception as e:
                oc_appio_internal_errors_total.labels(env, app_name).inc()
                logger.error(f'An error occurred: {e}')
    except Exception as e:
        oc_appio_internal_errors_total.labels(env, app_name).inc()
        logger.error(f'An error occurred: {e}')
