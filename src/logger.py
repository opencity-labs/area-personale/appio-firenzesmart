# Logger configuration
import logging
import os

LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')
LOG_FORMAT = os.environ.get('LOG_FORMAT', '%(asctime)s %(name)-32s %(levelname)-8s %(message)s')
logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)

appio_logger = logging.getLogger('app-io-proxy.appio')
file_logger = logging.getLogger('app-io-proxy.file')
